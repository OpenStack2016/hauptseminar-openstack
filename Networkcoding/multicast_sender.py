#! /usr/bin/env python
# encoding: utf-8

import argparse
import kodo
import os
import socket
import sys
import time
import struct

MCAST_GRP = '224.1.1.1'     # Multicast adress, so anybody can listen to the sender
MCAST_PORT = 5007           # A Port we chased really carefully

# ********defining default test parameters**********
DEFAULT_TEST_SZENARIO = 0

# duration of one TDMA slot
DEFAULT_SEND_TIME = 0.005

# Number of repetitions per loss probability and configuration
DEFAULT_NUMBER_OF_TESTS = 50
DEFAULT_LOSS_NUMBERS = 20

# packet size
SYMBOLS = 64
symbol_size = 1400


def main():
    # defining argument parser
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument(
        '--ip',
        type=str,
        help='The ip to send to.',
        default=MCAST_GRP)

    parser.add_argument(
        '--port',
        type=int,
        help='The port to send to.',
        default=MCAST_PORT)

    parser.add_argument(
        '--send_time',
        type=float,
        help='Time one guy is sending',
        default=DEFAULT_SEND_TIME)
    parser.add_argument(
        '--number_of_tests',
        type=int,
        help='How many times should be measured',
        default=DEFAULT_NUMBER_OF_TESTS)
    parser.add_argument(
        '--symbols',
        type=int,
        help='How many symbols will be send',
        default=SYMBOLS)
    parser.add_argument(
        '--number_loss_numbers',
        type=int,
        help='The amount of different loss ratios tested',
        default=DEFAULT_LOSS_NUMBERS)
    parser.add_argument(
        '--test_szenario',
        type=int,
        help='0 - iterating over sender - receiver loss ratio (rest fixed)',
        default=DEFAULT_TEST_SZENARIO)

    args = parser.parse_args()

    encoder_factory = kodo.FullVectorEncoderFactoryBinary(      # building an encoder factory to increase the speed
        max_symbols=args.symbols,                               # in which the actual encoder can be build
        max_symbol_size=symbol_size)

    sock = socket.socket(                                       # creating a socket to send and receive
        family=socket.AF_INET,
        type=socket.SOCK_DGRAM,
        proto=socket.IPPROTO_UDP)

    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)
    sock.bind(('', args.port))
    sock.setblocking(0)

    if args.test_szenario == 0:  # doing the test for increasing loss_ratio between sender and recceiver
        # number_loss_numbers tests with different loss probabilities, 2 configurations
        # without helper (normalization), with helper (gain diagram)
        for configuration_number in range(2):
            if configuration_number == 0:  # Tell the user in which mode the program actually tests
                print('Tests without helper')
            elif configuration_number == 1:
                print('Tests with helper')
            else:  # Pretty useless but classic
                print('nope')
            for loss_probaility in range(args.number_loss_numbers):  # Sending packets for all test in each loss_ratio
                send(sock, encoder_factory, args.ip, args.port, args.send_time, args.number_of_tests)
    else:  # doing the tests for increasing loss_ratio between helper and receiver
        # number_loss_numbers+1 tests
        print('Tests without helper')  # doing just one test because the helper is disabled, so an
        # increasing loss_ratio would not effect the transmission at all
        send(sock, encoder_factory, args.ip, args.port, args.send_time, args.number_of_tests)
        print('Test with helper')  # doing the tests for increasing loss_ratio between helper and receiver
        for loss_probaility in range(args.number_loss_numbers + 1):
            send(sock, encoder_factory, args.ip, args.port, args.send_time, args.number_of_tests)


def send(sock, encoder_factory, ip, port, send_time, number_of_tests):
    # ********************************************
    # this funciton sends based on TDMA (send_time) 
    # for the given number of tests (number_of_tests)  
    # Coded-Packets to the given adress (ip; port)
    # ********************************************

    # Setting up initial values
    gen = 0         # the number of the actual test(1 to number_of_tests for each loss ratio
    tb = 0.01       # the buffertime
    # The factories are used to build actual encoder
    encoder = encoder_factory.build()

    print('test started')           # tell the user when a new test starts
    while gen < number_of_tests:    # Stop if all tests are done
        gen += 1                    # Increase the number of the test, so it will run from 1 to number_of_tests
        # Assign the data buffer to the encoder to
        # produce encoded symbols
        encoder.set_const_symbols(os.urandom(encoder.block_size())) # Give the data to the encoder, random in our case
        address = (ip, port)                        # put the two parts of the destination adress in one value

        msg = ''                                    # (re)set the value message, which will be used later

        # send the synchronisation packet and define the timing slot start
        while True:
            try:
                sock.sendto(bytes('s', encoding='utf-8'), address)  # using try/except to catch possible failures
            except:
                continue
            break
        t0 = time.perf_counter()

        while True and msg != b'finished':

            if time.perf_counter() >= (t0 + 2 * send_time + 2 * tb):      # send next syncronisation message to start
                while True:                                               # the next timeslot
                    # syncing the other nodes after two time slots
                    try:
                        sock.sendto(bytes('s', encoding='utf-8'), address)
                    except:
                        continue
                    break
                t0 = time.perf_counter()                                  # reset the own timer after syncronisation
            # send packages until his time slot ends or the receiver finishes
            while time.perf_counter() < t0 + send_time and not msg == b'finished':
                # Generate an encoded packet
                gen_bytes = struct.pack('I', gen)               # convert the number of the actual test to bytes
                packet = gen_bytes + encoder.write_payload()    # generate packet and attach the number of the test
                msg = ''                                        # reset msg
                sock.sendto(packet, address)                    # send the actual packet

                try:
                    msg = sock.recv(10240)                      # listen if the receiver finished
                except:
                    pass
                time.sleep(0.00001)
                # wait some time to give the kernel the change to actually send the packet
            if time.perf_counter() > t0 + 100 * send_time:      # stop sending if the receiver went lost
                print('error!: no response from receiver')
                break


if __name__ == "__main__":                                      # check if this programm was initially called
    main()                                                      # execute main function
