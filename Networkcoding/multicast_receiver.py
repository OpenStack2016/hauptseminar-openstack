#! /usr/bin/env python
# encoding: utf-8

import argparse
import kodo
import socket
import struct
import time
import os
import random
import plotly.offline
import plotly.graph_objs
from math import sqrt

# set some default values
MCAST_PORT = 5007
DEFAULT_SEND_TIME = 0.005
DEFAULT_HELPER_LOSS_RATIO = 0.0
symbols = 64
symbol_size = 1400
DEFAULT_NUMBER_OF_TESTS = 50
DEFAULT_NUMBER_LOSS_NUMBERS = 20
DEFAULT_PLOT = 0
DEFAULT_TEST_SZENARIO = 0
DEFAULT_LOSS_RATIO = 0.8


def main():
    # define the argument parser
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument(
        '--output-file',
        type=str,
        help='Path to the file which should be received.',
        default='output_file')
    parser.add_argument(
        '--port',
        type=int,
        help='The port to send to.',
        default=MCAST_PORT)
    parser.add_argument(
        '--send_time',
        type=float,
        help='time one guy will send',
        default=DEFAULT_SEND_TIME)
    parser.add_argument(
        '--number_of_tests',
        type=int,
        help='How many times should be measured',
        default=DEFAULT_NUMBER_OF_TESTS)
    parser.add_argument(
        '--helper_loss_ratio',
        type=float,
        help='The ratio packets will from the helper get lost',
        default=DEFAULT_HELPER_LOSS_RATIO)
    parser.add_argument(
        '--loss_ratio',
        type=float,
        help='The ratio packets will from the sender get lost',
        default=DEFAULT_LOSS_RATIO)
    parser.add_argument(
        '--number_loss_numbers',
        type=int,
        help='The amount of different loss ratios tested',
        default=DEFAULT_NUMBER_LOSS_NUMBERS)
    parser.add_argument(
        '--plot',
        type=int,
        help='0 - plot gain diagram, 1 - plot transmission time diagram',
        default=DEFAULT_PLOT)
    parser.add_argument(
        '--test_szenario',
        type=int,
        help='0 - iterating over sender - receiver loss ratio (rest fixed), 1 - iterating over helper-receiver loss ratio (rest fixed)',
        default=DEFAULT_TEST_SZENARIO)

    args = parser.parse_args()

    decoder_factory = kodo.FullVectorDecoderFactoryBinary(
        max_symbols=symbols,
        max_symbol_size=symbol_size)
    # define lists so values can be append later. The lists will be used to store/calculate the data for the plots
    ylist = []      # maybe ylist is unnecessary but im not sure
    y1list = []
    y2list = []
    xlist = []
    sd1 = []
    sd2 = []
    sd3 = []
    zlist = []

    try:
        os.remove('data.txt')   # remove the written data from previous experiments if available
    except:
        pass

    if args.test_szenario == 0:     # check which mode should be used
        # test szenario: iteration over sender-receiver loss ratio
        # only source sending    
        for loss_number in range(args.number_loss_numbers):     # do the wished number of test without an helper
            # to create an reference poin
            ylist = receive(args.send_time, args.port, args.number_of_tests, decoder_factory,
                            loss_number / args.number_loss_numbers, 1)
            # write it down
            outfile(ylist, 'without helper', args.number_of_tests, loss_number / args.number_loss_numbers, 1)
            y1list.append(sum(ylist) / len(ylist))                      # calculate the average value of the latest test
            varianz = [(x - y1list[loss_number]) ** 2 for x in ylist]   # and the variance
            sd1.append(sqrt(sum(varianz) / len(varianz)))               # and the standard deviation
            xlist.append(loss_number / args.number_loss_numbers)        # and nice labels for the x-aches
        print('Test with helper')                                       # tell the user that the helper is now working
        # source and helper sending
        for loss_number in range(args.number_loss_numbers):             # do the test again, now serios(with helper)
            ylist = receive(args.send_time, args.port, args.number_of_tests, decoder_factory,
                            loss_number / args.number_loss_numbers, args.helper_loss_ratio)
            # write the results down
            outfile(ylist, 'with helper', args.number_of_tests, loss_number / args.number_loss_numbers,
                    args.helper_loss_ratio)
            y2list.append(sum(ylist) / len(ylist))                      # calculate the average value
            varianz = [(x - y2list[loss_number]) ** 2 for x in ylist]   # and the variance
            sd2.append(sqrt(sum(varianz) / len(varianz)))               # and the standard deviation
    else:                           # well seems like the user chased the second mode. don't cry, do your job
        # test scenario iterating over helper-receiver loss ratio
        # only source sending
        # do a single test for the reference
        ylist = receive(args.send_time, args.port, args.number_of_tests, decoder_factory, args.loss_ratio, 1)
        outfile(ylist, 'without helper', args.number_of_tests, args.loss_ratio, 1)

        for loss_number in range(args.number_loss_numbers + 1):         # make if look like you did a lot
            y1list.append(sum(ylist) / len(ylist))                      # calculate the average value of the latest test
            varianz = [(x - y1list[loss_number]) ** 2 for x in ylist]   # and the variance
            sd1.append(sqrt(sum(varianz) / len(varianz)))               # and the standard deviation
            xlist.append(loss_number / args.number_loss_numbers)        # and nice labels for the x-aches
        print('helper started')                                         # tell the user that the helper is now working
        # source and helper sending
        for loss_number in range(args.number_loss_numbers + 1):         # do as many tests as necessary
            # and increase the loss ratio
            ylist = receive(args.send_time, args.port, args.number_of_tests, decoder_factory,
                            args.loss_ratio, loss_number / args.number_loss_numbers)
            # write the result down
            outfile(ylist, 'with helper', args.number_of_tests, args.loss_ratio, loss_number / args.number_loss_numbers)
            y2list.append(sum(ylist) / len(ylist))                      # calculate the average value of the latest test
            varianz = [(x - y2list[loss_number]) ** 2 for x in ylist]   # and the variance
            sd2.append(sqrt(sum(varianz) / len(varianz)))               # and the standard deviation

    for i in range(len(y1list)):                                        # calculate the gain causes by the helper
        zlist.append(y1list[i] / y2list[i] - 1)
        sd3.append(sqrt((sd1[i] / y2list[i]) ** 2 + (y1list[i] * sd2[i] / y2list[i] ** 2) ** 2))
        # and the standard deviation in this case
    if not args.plot:                           # chose which graph you would like to plot
        # plot gain diagram
        plot(xlist, zlist, sd3)                 # plot the gain graph
    else:
        # plot time-loss-ratio-diagram
        plot2(xlist, y1list, sd1, y2list, sd2)  # plot the time graph


def receive(t_send, port, number_of_tests, decoder_factory, loss_ratio, helper_loss_ratio):
    # set some initial values
    cur_gen = 0
    ylist = []
    tb = 0.01
    # create a socket to receive and send something
    rsock = socket.socket(
        family=socket.AF_INET,
        type=socket.SOCK_DGRAM,
        proto=socket.IPPROTO_UDP)
    rsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)     # configure it
    rsock.bind(('', port))                                          # bind it
    mreq = struct.pack("4sl", socket.inet_aton('224.1.1.1'), socket.INADDR_ANY)
    rsock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)     # love it

    print('test started')                                           # tell the user your doing something

    while cur_gen < number_of_tests:                                # stop doing something if your done
        decoder = decoder_factory.build()                           # create the decoder
        rx = ''                                                     # reset the rx value

        # syncing the time slots
        rsock.setblocking(1)                                        # set the socket blocking
        while not rx == b's':                                       # wait for an synchronisation signal
            rx = rsock.recv(10240)                                  # receive the synchronisation signal
        rx = ''
        rsock.setblocking(0)                                        # set the socket unblocking
        t0 = time.perf_counter()                                    # (re)set the timer for the time slots
        tstart = t0                                                 # save the date

        while not decoder.is_complete():                            # check if your done

            if time.perf_counter() < t0 + t_send:
                # setting variable to analyse if the helper is sending
                h_packet = False
            if t0 + t_send + tb < time.perf_counter() < t0 + 2 * t_send + tb:
                # source sending
                h_packet = True
            rx = ''                                                 # reset the rx value
            try:
                rx = rsock.recv(10240)                              # try to receive something
            except:
                continue

            if rx == b's':                                          # if you got an synchronisation signal
                t0 = time.perf_counter()                            # reset the timer
                continue

            cur_gen = struct.unpack('I', rx[:4])[0]                 # if not get the number of the test from the packet
            packet = rx[4:]                                         # save the packet

            if h_packet:                                            # check if the packet was sendet by the helper
                if random.random() > helper_loss_ratio:             # check if your allowed to receive the packet
                    decoder.read_payload(packet)                    # get the information from the packet
            else:                                                   # the packet comes from the sender
                if random.random() > loss_ratio:                    # check if your allowed to receive the packet
                    decoder.read_payload(packet)                    # get the information from the packet
        transfer_time = time.perf_counter() - tstart          # calculate how much time you needed for this transmission
        rsock.sendto(bytes('finished', encoding='utf-8'), ('10.0.0.8', port))   # say the sender your done
        rsock.sendto(bytes('finished', encoding='utf-8'), ('10.0.0.4', port))   # say the helper your done
        ylist.append(transfer_time)                                 # append the result to an list
    return ylist


def plot(xlist, ylist, sd):                                         # plot a graph with just a single trace
    trace1 = plotly.graph_objs.Scatter(                             # generate the trace
        x=xlist,                                                    # by using the data for x aches
        y=ylist,                                                    # by using the data for y aches
        error_y={'type': 'data', 'array': sd, 'visible': True},     # by using the error data
        name="Without helper",                                      # give the trace a name, useless for just one trace
        marker=dict(color="rgb(26,118,255)"))                       # set the color of the trace
    plotly.offline.plot({                                           # plot the graph
        "data": [trace1],                                           # give the data to the plot
        "layout": {"title": "Gain - Loss ratio - Diagram",          # set the title of the diagram
                   "xaxis": {"title": "Loss ratio"},                # set the title for the x aches
                   "yaxis": {"title": "Gain in Transmission Time"}, # set the title for teh y aches
                   }

    })                                                              # finally your done, grep a beer


def plot2(x1list, y1list, sd1, y2list, sd2):                        # plot a graph with two traces
    trace1 = plotly.graph_objs.Scatter(                             # generate the first trace
        x=x1list,                                                   # come on you know how to do this
        y=y1list,
        error_y={'type': 'data', 'array': sd1, 'visible': True},
        name="Without helper",
        marker=dict(color="rgb(26,118,255)")
    )
    trace2 = plotly.graph_objs.Scatter(                             # generate the second trace
        x=x1list,
        y=y2list,
        error_y={'type': 'data', 'array': sd2, 'visible': True},
        name="With helper",
        marker=dict(color="rgb(107,107,107)")
    )
    plotly.offline.plot({                                           # plot the diagram
        "data": [trace1, trace2],
        "layout": {"title": "Transmission Time - Loss ratio - Diagram",
                   "xaxis": {"title": "Loss ratio"},
                   "yaxis": {"title": "Time neaded by each transmission in 1/s"},
                   }
    })


def outfile(data, helpermode, number_of_tests, loss_ratio, helper_loss_ratio):  # write your results to a file
    fwrite = open('data.txt', 'a')                                              # open a file
    fwrite.write(                                                               # write the title down
        '\n helpermode: {}, loss ratio (sender/helper): {}/{}, \n'.format(helpermode, loss_ratio, helper_loss_ratio))
    for line in range(len(data)):                                               # write the data down
        fwrite.write(str(data[line]))
        fwrite.write(' ')
        if line == number_of_tests:
            fwrite.write('\n')
    fwrite.close()                                                              # your done, close the document


if __name__ == "__main__":
    main()
