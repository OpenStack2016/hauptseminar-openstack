#! /usr/bin/env python
# encoding: utf-8

import argparse
import kodo
import socket
import time
import struct
import random

# define some default values
MCAST_GRP = '224.1.1.1'
MCAST_PORT = 5007
DEFAULT_SEND_TIME = 0.005
DEFAULT_LOSS_RATIO = 0.0

symbols = 64
symbol_size = 1400
DEFAULT_NUMBER_OF_TESTS = 50
DEFAULT_NUMBER_LOSS_NUMBERS = 20

DEFAULT_TEST_SZENARIO = 0


def main():
    # define argument-parser
    parser = argparse.ArgumentParser(description=main.__doc__)

    parser.add_argument(
        '--send_time',
        type=float,
        help='time one guy will send',
        default=DEFAULT_SEND_TIME)

    parser.add_argument(
        '--ip',
        type=str,
        help='The ip to send to.',
        default=MCAST_GRP)

    parser.add_argument(
        '--port',
        type=int,
        help='The port to send to.',
        default=MCAST_PORT)

    parser.add_argument(
        '--loss_ratio',
        type=float,
        help='The ratio packets from scource will get lost',
        default=DEFAULT_LOSS_RATIO)

    parser.add_argument(
        '--number_of_tests',
        type=int,
        help='How many times should be measured',
        default=DEFAULT_NUMBER_OF_TESTS)

    parser.add_argument(
        '--number_loss_numbers',
        type=int,
        help='The amount of different loss ratios tested',
        default=DEFAULT_NUMBER_LOSS_NUMBERS)

    parser.add_argument(
        '--test_szenario',
        type=int,
        help='0 - iterating over sender - receiver loss ratio (rest fixed)',
        default=DEFAULT_TEST_SZENARIO)

    args = parser.parse_args()

    decoder_factory = kodo.FullVectorDecoderFactoryBinary(     # build an decoder factory to increase the speed
        max_symbols=symbols,                                   # in which a decoder can be build
        max_symbol_size=symbol_size)

    sock = socket.socket(                                      # build an socket to send and receive packets
        family=socket.AF_INET,
        type=socket.SOCK_DGRAM,
        proto=socket.IPPROTO_UDP)

    # create the socket for sending and receiving purpose
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)  # set some socket options
    sock.bind(('', args.port))                                  # bind the socket so an port to get all packets at this
    mreq = struct.pack("4sl", socket.inet_aton('224.1.1.1'), socket.INADDR_ANY)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

    if args.test_szenario == 0:                                 # check if tho increasing loss_ratio should be between
        # sender and receiver or helper and receiver
        # 2 configurations (with and without sender), number_loss_numbers different loss ratios
        for configuration_number in range(2):
            for loss_number in range(args.number_loss_numbers):     # do as many tests as expected
                relay_policy(args.send_time, args.number_of_tests, sock, decoder_factory, args.ip, args.port,
                             args.loss_ratio)
            print('Test with helper')                               # say the user, that you now really start working
    else:
        # number_loss_numbers+1 different tests                     listen in some test
        relay_policy(args.send_time, args.number_of_tests, sock, decoder_factory, args.ip, args.port, args.loss_ratio)
        print('Test with helper')                                   # tell the user your now doing something
        for loss_number in range(args.number_loss_numbers + 1):     # do as many tests as expected
            relay_policy(args.send_time, args.number_of_tests, sock, decoder_factory, args.ip, args.port,
                         args.loss_ratio)


def relay_policy(send_time, number_of_tests, sock, decoder_factory, ip, port, loss_ratio):
    # ***************************************************************************
    # this function does the helper work, it forwards number_of_tests transmissions
    # after it got all the data, the end of every test needs to be signalized  
    # by a b'finished' packet, the helper needs to be synchronized with b's'
    # after two time slots
    # ***************************************************************************
    # set some initial values
    cur_gen = 0                 # set the current received generation to a start value
    tb = 0.01                   # set the buffer time
    adress = (ip, port)         # put the receiver information in one value
    print('test started')       # tell the user you have started doing something

    while cur_gen < number_of_tests:        # stop working if you did enough
        decoder = decoder_factory.build()   # create the guy who's actually working
        rx = ''                             # reset the rx value
        sock.setblocking(0)                 # set the socket unblocking
        while not rx == b's':               # wait for an synchronisation signal
            try:
                rx = sock.recv(10240)       # receive the actual synchronisation signal
            except:
                continue
        rx = ''                             # reset rx value to not bring the next stuff in trouble
        t0 = time.perf_counter()

        while not rx == b'finished':        # if the receiver as not finished
            while time.perf_counter() < t0 + send_time and not rx == b'finished': # receive if the time for this is not
                #  over or the receiver has finished
                rx = ''                     # reset the rx value again
                try:
                    rx = sock.recv(10240)   # try to receive something
                except:
                    continue
                if rx == b's':              # reset the timer, if you received a synchronisation signal
                    t0 = time.perf_counter()
                    continue
                if not rx == b'finished':   # don't decode the packet if its just the message
                    #  from the receiver, that his done
                    cur_gen = struct.unpack('I', rx[:4])[0] # get the number of the actual test from the received packet
                    packet = rx[4:]                         # store the packet
                    if random.random() > loss_ratio:        # check if you allowed to receive the packet
                        decoder.read_payload(packet)        # actually receive the packet/get the information out

            while t0 + send_time + tb < time.perf_counter() < t0 + 2 * send_time + tb and not rx == b'finished' and not rx == b's':
                # sending loop - if the helper has all data, the packets are sent here to the given ip
                try:
                    rx = sock.recv(10240)                   # try to receive
                except:
                    pass

                if decoder.is_complete():                   # start sending if the decoder is complete
                    gen_bytes = struct.pack('I', cur_gen)   # convert the number of the test to bytes
                    packet = gen_bytes + decoder.write_payload() # generate the packet and attach the number of the test
                    sock.sendto(packet, adress)             # send the packet
                    time.sleep(0.00001)                     # give the kernel some time to really send the packet

            if rx == b's' or rx == b'finished':             # reset the timer if there was an sync or finisched
                # during the previous time slots
                t0 = time.perf_counter()
                continue
            try:
                rx = sock.recv(10240)                       # check if there is now an packet
            except:
                pass
            if rx == b's' or rx == b'finished':             # reset the timer if there was an sync or finished
                # print(rx)
                t0 = time.perf_counter()
                continue


if __name__ == "__main__":
    main()
