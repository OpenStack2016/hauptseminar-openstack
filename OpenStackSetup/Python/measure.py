import methods_general as mg
import methods_measure as mm

image_name = 'CirrOS_0.3.2'
flavor_name = 'm1.nano'
n = 30
interval = '95%'
t = 2.042

medians_init, sigmas_init, boundaries_init, medians_boot, sigmas_boot, boundaries_boot, time_values_f, time_values_i, time_values_b = mm.measure(image_name, flavor_name, n, interval, t)

mg.plot(medians_init, sigmas_init, boundaries_init, n, image_name, interval, False)
mg.plot(medians_boot, sigmas_boot, boundaries_boot, n, image_name, interval, True)

mg.write_results(image_name, flavor_name, n, interval, t, time_values_f, time_values_i, time_values_b,  medians_init, sigmas_init, boundaries_init, medians_boot, sigmas_boot, boundaries_boot)