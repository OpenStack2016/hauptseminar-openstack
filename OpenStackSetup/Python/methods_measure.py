import timeit
import time
import subprocess
from unicodedata import normalize
import numpy

import methods_general as mg

def measure(image_name, flavor_name, n, interval, t):

	# authenticate, get nova + glance object
	nova, glance = mg.authenticate()

	# delete instances, if existing
	#mg.delete_instances(nova)

	# create lists for storing calculated medians, standard deviations and confidence intervals
	medians_init = []
	sigmas_init = []
	boundaries_init = []
	
	medians_boot = []
	sigmas_boot = []
	boundaries_boot = []


########### measure function time ############

	# create list for storing time values
	time_values_f = []

	i = 1
	print 'Measure Function Time'

	# repeat measurement n times
	while i <= n:

		# number instances
		instance_name = image_name+' '+str(i)
		
		# add measured time to list
		time_values_f.append(measure_func_time(mg.create_instance, nova, instance_name, image_name, flavor_name, False, False))

		print image_name+' '+str(i)+' finished!'
		mg.delete_instance(nova, instance_name)
		i = i+1

	# calculate median, standard deviation and confidence interval
	median = numpy.median(time_values_f)
	sigma = numpy.std(time_values_f, ddof=1)
	boundary = mg.calc_interval(sigma, n, t)

	# add calculated values to corresponding list
	medians_init.append(median)
	sigmas_init.append(sigma)
	boundaries_init.append(boundary)

	print('Function Time Measured')


########## measure initialization time ##########

	# create list for storing time values
	time_values_i = []

	i = 1
	print 'Measure Initialization Time'

	# repeat measurement n times
	while i <= n:

		# number instances
		instance_name = image_name+' '+str(i)

		mg.create_instance(nova, instance_name, image_name, flavor_name, False, False)

		# add measured time to list
		time_values_i.append(measure_init(nova, i, instance_name))

		print image_name+' '+str(i)+' finished!'
        	mg.delete_instance(nova, instance_name)
		i = i+1

	# calculate median, standard deviation and confidence interval
	median = numpy.median(time_values_i)
	sigma = numpy.std(time_values_i, ddof=1)
	boundary = mg.calc_interval(sigma, n, t)

	# add calculated values to corresponding list
	medians_init.append(median)
	sigmas_init.append(sigma)
	boundaries_init.append(boundary)

	print('Initialization Time Measured')


########## measure boot time (3 variations) ###########

	# create list for storing time values of all 5 varations
	time_values_b = []


# 1. start: when instance is listed as 'active', stop: when host receives message from instance

	# create list for storing time values
	time_values_b1 = []

	i = 1
	print 'Measure Boot Time (NC)'

	while i <= n:

		# number instances
		instance_name = image_name+' '+str(i)

		mg.create_instance(nova, instance_name, image_name, flavor_name, True, False)

		# add measured time to list
		time_values_b1.append(measure_boot_time_nc(nova, instance_name))

		print image_name+' '+str(i)+' finished!'
        	mg.delete_instance(nova, instance_name)
        	i = i+1

	# calculate median, standard deviation and confidence interval
	median = numpy.median(time_values_b1)
	sigma = numpy.std(time_values_b1, ddof=1)
	boundary = mg.calc_interval(sigma, n, t)

	# add calculated values to corresponding list
	medians_boot.append(median)
	sigmas_boot.append(sigma)
	boundaries_boot.append(boundary)

	time_values_b.append(time_values_b1)

	print('Boot Time Measured (NC)')


# 2. start: when instance is listed as 'active', stop: instance sends current date to host

	# create list for storing time values
	time_values_b2 = []

	i = 1
	print 'Measure Boot Time (NC With Date)'

	while i <= n:

		# number instances
		instance_name = image_name+' '+str(i)

		mg.create_instance(nova, instance_name, image_name, flavor_name, True, True)

		# add measured time to list
		time_values_b2.append(measure_boot_time_nc_with_date(nova, instance_name))

		print image_name+' '+str(i)+' finished!'
        	mg.delete_instance(nova, instance_name)
		i = i+1

	# calculate median, standard deviation and confidence interval
	median = numpy.median(time_values_b2)
	sigma = numpy.std(time_values_b2, ddof=1)
	boundary = mg.calc_interval(sigma, n, t)

	# add calculated values to corresponding list
	medians_boot.append(median)
	sigmas_boot.append(sigma)
	boundaries_boot.append(boundary)

	time_values_b.append(time_values_b2)

	print('Boot Time Measured (NC With Date)')


# 3. extract boot time from logfile 

	# create list for storing time values
	time_values_b3 = []

	i = 1
	print 'Measure Boot Time (Logfile)'

	while i <= n:

		# number instances
		instance_name = image_name+' '+str(i)

		mg.create_instance(nova, instance_name, image_name, flavor_name, True, False)

		# add measured time to list
		# extraction algorithm depends on image
		if "cirros" in image_name.lower():
			time_values_b3.append(measure_boot_time_log_cirros(nova, instance_name))
		elif "ubuntu" in image_name.lower():
			time_values_b3.append(measure_boot_time_log_ubuntu(nova, instance_name))

		print image_name+' '+str(i)+' finished!'
        	mg.delete_instance(nova, instance_name)
		i = i+1

	# calculate median, standard deviation and confidence interval
	median = numpy.median(time_values_b3)
	sigma = numpy.std(time_values_b3, ddof=1)
	boundary = mg.calc_interval(sigma, n, t)

	# add calculated values to corresponding list
	medians_boot.append(median)
	sigmas_boot.append(sigma)
	boundaries_boot.append(boundary)

	time_values_b.append(time_values_b3)

	print('Boot Time Measured (Logfile)')

	return medians_init, sigmas_init, boundaries_init, medians_boot, sigmas_boot, boundaries_boot, time_values_f, time_values_i, time_values_b


def wrapper(func, *args, **kwargs):
	
	# method for wrapping a function with parameter into one variable
    def wrapped():
        return func(*args,**kwargs)

    return wrapped
    

def measure_func_time(func, *args, **kwargs):

	# uses the wrapper method from above in order to give it as parameter to timeit-method
    wrap = wrapper(func,*args,**kwargs)

	# method for messure the given method
    func_time = timeit.timeit(wrap,number=1)

    return func_time


def measure_init(nova, i, instance_name):

	# measure start time
	start = time.time()

	# waits until instance is listed as 'active'
	stop = mg.wait_until_active(nova, instance_name)

	# calculate time
	init_time = stop - start

	return init_time


def measure_boot_time_nc(nova, instance_name):

	# waits until instance is listed as 'active'
	start = mg.wait_until_active(nova, instance_name)

	# listen on port 10000
	sub = subprocess.Popen(['nc', '-l', '-p', '10000'])

	# wait until subprocess terminated (instance sends 'exit' to host)
	sub.wait()

	# measure end time
	stop = time.time()

	# calculate time
	boot_time = stop - start

	return boot_time


def measure_boot_time_nc_with_date(nova, instance_name):

	# waits until instance is listed as 'active'
	start = mg.wait_until_active(nova, instance_name)

	# listen on port 10000
	sub = subprocess.Popen(['nc', '-l', '-p', '10000'], stdout = subprocess.PIPE)

	# wait for input (instance sends current date to host)
	stop, err = sub.communicate()

	# calculate time	
	boot_time = float(int(stop)) - start

	return boot_time


def measure_boot_time_log_cirros(nova, instance_name):

	# listen on port 10000
	sub = subprocess.Popen(['nc', '-l', '-p', '10000'])

	# wait until subprocess terminated (instance sends 'exit' to host)
	sub.wait()

	# find corresponding instance -> its id is needed for accessing logfile
	instances = nova.servers.list()
	for instance in instances:
		if instance.name == instance_name:
			instance_id = instance.id

	# wait until booting is complete (otherwise the logfile would be incomplete)
	while normalize('NFKD', nova.servers.get_console_output(instance_id)).find('uptime=') == -1:
		pass

	# convert unicode to string
	log_string = normalize('NFKD', nova.servers.get_console_output(instance_id))

	# split string to get the time
	list1 = log_string.split('uptime=')
	list2 = list1[-1].split(' ')

	# read time
	boot_time = list2[0]

	return float(boot_time)


def measure_boot_time_log_ubuntu(nova, instance_name):

	# listen on port 10000
	sub = subprocess.Popen(['nc', '-l', '-p', '10000'])

	# wait until subprocess terminated (instance sends 'exit' to host)
	sub.wait()

	# find corresponding instance -> its id is needed for accessing logfile
	instances = nova.servers.list()
	for instance in instances:
		if instance.name == instance_name:
			instance_id = instance.id

	# wait until booting is complete (otherwise the logfile would be incomplete)
	while normalize('NFKD', nova.servers.get_console_output(instance_id)).find('-----END SSH HOST KEY KEYS-----') == -1:
		pass

	# convert unicode to string
	log_string = normalize('NFKD', nova.servers.get_console_output(instance_id))

	# split string to get the time
	list1 = log_string.split('-----END SSH HOST KEY KEYS-----')
	list2 = list1[-1].split('Up ')
	list3 = list2[-1].split(' ')

	# read time
	boot_time = list3[0]

	return float(boot_time)