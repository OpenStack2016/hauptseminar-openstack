from keystoneauth1.identity import v3
from keystoneauth1 import session
from keystoneauth1 import loading
from keystoneclient.v3 import client as ksclient
from novaclient import client as nvclient
from glanceclient import client as glclient
import os
import subprocess
import timeit
import time
import csv
import plotly.offline as py
import plotly.graph_objs as go
import socket
import fcntl
import struct

link_keypair = '/home/entmpr11/hauptseminar-openstack/keypair1.pub'
link_cf_date = '/home/entmpr11/OpenStack/final/config_date'
link_cf_wait = '/home/entmpr11/OpenStack/final/config_wait'


def get_creds():

	# get credentials from environment
	d = {}
	d['username'] = os.environ['OS_USERNAME']
	d['password'] = os.environ['OS_PASSWORD']   
	d['auth_url'] = os.environ['OS_AUTH_URL'] 
	d['project_id'] = os.environ['OS_TENANT_ID']
	return d


def get_ip_address(ifname):

	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

	return socket.inet_ntoa(fcntl.ioctl(
		s.fileno(),
		0x8915,  # SIOCGIFADDR
		struct.pack('256s', ifname[:15])
	)[20:24])


def authenticate():

	# get credentials
	creds = get_creds()

	# authenticate against keystone (identity) endpoint
	auth_ks = v3.Password(**creds)
	sess_ks = session.Session(auth=auth_ks)
	keystone = ksclient.Client(session=sess_ks)

	# authenticate against nova (compute) endpoint 
	loader = loading.get_plugin_loader('password')
	auth_nv = loader.load_from_options(**creds)
	sess_nv = session.Session(auth=auth_nv)
	nova = nvclient.Client('2', session=sess_nv)

	# get ip address, needed for glance (image) endpoint 
	ip = get_ip_address('eth0')

	# construct url for glance (image) endpoint
	glance_endpoint = 'http://'+ip+':9292'
	
	# authenticate against glance (image) endpoint
	glance = glclient.Client('1', glance_endpoint, session=sess_ks)

	return nova, glance


def find_securityGroup(nova, name):
 
	return nova.security_groups.find(name=name)


def create_securityGroupRule(nova, group_id, ip_protocol, from_port, to_port): 
	'''
	param nova: 		Nova Client
	param group_id:		ID of security group where new security rule should be created
	param ip_protocol:	network protocol
	param form_port:	source port
	param to_port:		destination port
	'''

	return nova.security_group_rules.create(group_id, ip_protocol=ip_protocol, from_port=from_port, to_port=to_port)
    

def delete_securityGroupRule(nova, rule_id):

	nova.security_group_rules.delete(rule_id)


def delete_all_ssh_and_icmp(nova, security_group_name):

	# find corresponding security group
	g = find_securityGroup(nova, security_group_name)

	# delete ssh and icmp rules for this group
	for i in g.rules:

        	if i[u'ip_protocol']==u'icmp' or i[u'ip_protocol']==u'tcp':

            		delete_securityGroupRule(nova,i[u'id'])

    	return find_securityGroup(nova, security_group_name)
    

def import_keyPairs(nova, key_path, key_name):
	'''
	param nova: Nova Client
	param key_path: directory of private keypair
	param key_name: name for keypair in OpenStack
	'''

	# open keypair
    	with open(os.path.expanduser(key_path)) as file: 
     		key = file.read()

	# add keypair
    	nova.keypairs.create(key_name, key)
    

def delete_keyPair(nova, keypair_id):

    	nova.keypair.delete(keypair_id)
    

def delete_all_keyPairs(nova):

	# get a list of all keypairs
   	keypairs = nova.keypairs.list()

	# delete all keypairs
   	for keypair in keypairs:

         	nova.keypairs.delete(keypair.id)


def change_file_rights():
   
    	subprocess.Popen('sudo chmod 600 '+link_keypair, shell=True, executable="/bin/bash")   


def create_image(glance, image_file, image_name, is_public=True, disk_format="qcow2", container_format="bare"):
	'''
	param glance:			Glance Client
	param image_file:		Directory of Image File
	param image_name:		Name for Image in OpenStack
	param is_public:		Should Image be public? (default: True)
	param disk_format:		Disk format
	param container_format:	Container format of disk image
	'''
	

    with open(image_file) as fimage:

        glance.images.create(name=image_name, is_public=is_public, disk_format=disk_format, container_format=container_format, data=fimage)


def delete_image(glance, image_name):

   	try:
        	image = glance.images.find(name=image_name)
    	except:
        	pass # kein Image mit dem Imagenamen gefunden! Es soll nichts passieren..
   	else:
        	image.delete()
        


def delete_all_instances(nova):

	# as long there are severs in the list this loop is executed
	while len(nova.servers.list()) != 0:

		# delete only active instances, deleting spawning instances produces error
		if len(nova.servers.list(search_opts={'status': 'ACTIVE'})) != 0:

			for instance in nova.servers.list(search_opts={'status': 'ACTIVE'}):
				nova.servers.delete(instance.id)		    

def delete_instance(nova, instance_name):
	
	while True:
		success = False
		i_list = nova.servers.list(search_opts={'status': 'ACTIVE'})
		for i in i_list:
			if i.name == instance_name:
				nova.servers.delete(i.id)
				success = True
				break
		if success==True:
			break

def create_instance(nova, instance_name, image_name, flavor_name, boot, date, floating_ip=None, network_name='demo'):
	'''
	param nova:				Nova Client
	param instance_name: 	Name of instance
	param image_name: 		Name of image file
	param flavor_name: 		Name of flavor
	param boot:				Triggers boot-time-measurement
	param date:				Triggers boot-time-measurement with date
	param floating_ip		Assign a floating ip to the new instance (default: None)
	param network_name		Network name of network where the new instance should be created (dafault: demo)
	'''
	
    
    # define essential parameters
    flavor = nova.flavors.find(name=flavor_name)
    image = nova.images.find(name=image_name)
        
    network = nova.networks.find(label=network_name)
    nics = [{'net-id':network.id}]
        
    # if boot time is measured, a config file will be executed after booting
    if boot == True:
            
        # if method measure_boot_time_3 or measure_boot_time_4 is used, the date will be sent after booting
        if date == True:
                
            config_file = open(link_cf_date)
            instance = nova.servers.create(name=instance_name,
                                            image=image,
                                            flavor=flavor,
                                            userdata=config_file,
                                            nics = nics)
            
        # here the date will not be sent
        elif date == False:
            
            config_file = open(link_cf_wait)
            instance = nova.servers.create(name=instance_name,
                                            image=image,
                                            flavor=flavor,
                                            userdata=config_file,
                                            nics = nics)
    
    # create instance without config file for other measurement types
    elif boot == False:
        
        instance = nova.servers.create(name=instance_name,
                                       image=image,
                                       flavor=flavor,
                                       nics = nics)
                                       
    # assigns floating ip to instance if desired
    if floating_ip != None:
        assign_floating_ip(nova, instance_name, floating_ip)


def wait_until_active(nova, instance_name):

	# set variable for leaving loop
	exit = False

	# wait until instance is listed as 'active'
	while exit == False:

		# list all 'active' instances
        	servers = nova.servers.list(search_opts={'status': 'ACTIVE'})

		# search in list for corresponding instance
        	for server in servers:

			# measure end time and leave loop when instance has been found
                	if server.name == instance_name:

                        	r_time = time.time()
                        	exit = True

	return r_time


def create_floating_ip(nova):
	
	# creates a new floating ip from the pool (public)
        floating_ip = nova.floating_ips.create(nova.floating_ip_pools.list()[0].name)


def assign_floating_ip(nova, instance_name, floating_ip):

	# waits until instance is listed as 'active'
	wait_until_active(nova, instance_name)

	# finds instance with specific name
	instance = nova.servers.find(name=instance_name)

	# adds floating ip to instance
	instance.add_floating_ip(floating_ip)


def remove_floating_ip(nova, instance_name, floating_ip):

	# finds instance with specific name
	instance = nova.servers.find(name=instance_name)

	# remove ip from instance
	nova.servers.remove_floating_ip(instance.id, floating_ip)

	
def delete_floating_ip(nova, floating_ip):

	# list all floating ips	
	list = nova.floating_ips.list()

	# search and delete specific floating ip
	for entry in list:
		try:
			if entry.ip==floating_ip:
				nova.floating_ips.delete(entry.id)
				break
		except:
			pass
		
	
def delete_all_floating_ips(nova):
	
	# list all floating ips
	list = nova.floating_ips.list()

	# delete all floating ips
	for entry in list:
		try:
			nova.floating_ips.delete(entry.id)
		except:
			pass


def get_ip(nova, instance_name):

	# finds instance with specific name
	instance = nova.servers.find(name=instance_name)

	# gets all ips addresses
	ip = nova.servers.ips(instance.id)

	# extract ip address
	return ip[u'private'][0][u'addr']


def get_floating_ip(nova, instance_name):

	# finds instance with specific name
	instance = nova.servers.find(name=instance_name)

	# gets all ips addresses
	floating_ip = nova.servers.ips(instance.id)

	# extract floating ip address
	return floating_ip[u'private'][2][u'addr']


def how_many_free_fips(nova):
        '''Checks the project floating ip address pool for unassimilated ip addresses and returns the number of them.'''

        # gets list with all floating ip addresses in the pool
        flist = nova.floating_ips.list()

        # counter for free floating ip addresses

        # for-Loop for counting the free ip addresses
        counter = 0
        for x in flist:
                if x.instance_id==None:
                        counter += 1

        return counter

def print_free_fips(nova):
        '''Prints all unassimilated floating ip addresses.'''

        # gets list with all floating ip addresses in the pool
        flist = nova.floating_ips.list()

        # counter for free floating ip addresses

        # for-Loop for counting the free ip addresses
        counter = 0
        for x in flist:
                if x.instance_id==None:
                        print (x.ip)

def create_until_i_free_fips(nova,number):
        '''Allocate as many floating ips for a project until enough (number) ip addresses are free for instances.'''

        # counts number of not-used floating ip addresses and calculates the number of missing ips
        missing_ips = number - how_many_free_floating_ip(nova)

        # check if there are missing floating ips, if yes, then allocate more ips until number reached
        if missing_ips>0:
                for i in range(missing_ips):
                        create_floating_ip(nova)

        # prints all free floating ips of the IP Pool
        print_free_fips(nova)
        
        
def plot(medians, boundaries, n, image_name, interval, boot):

	# name x-Axis
	if boot == False:
		x = ['Func', 'Init']
	elif boot == True:
		x = ['NC', 'NC With Date', 'Logfile']

	# create bar for medians with confidence interval
	data = [go.Bar(
		x=x,
		y=medians,
		name='Median + CI '+interval,
		error_y=dict(type='data', array=boundaries, visible=True)
	)]

	# define layout
	layout = go.Layout(showlegend = True,
			#barmode = 'group',
			title="Boot Time Measurement (n = %i)\nImage = %s" %(n, image_name),
			xaxis=dict(
        			title='Type'
        		),
			yaxis=dict(
				title='Seconds'
			)
	)

	# create figure and plot it
	fig = go.Figure(data=data, layout=layout)
	if boot == False:
		py.plot(fig, filename = 'chart_'+image_name+'_n='+str(n)+'_init.html')
	elif boot == True:
		py.plot(fig, filename = 'chart_'+image_name+'_n='+str(n)+'_boot.html')


def write_results(image_name, flavor_name, n, interval, t, time_values_f, time_values_i, time_values_b, medians_init, sigmas_init, boundaries_init, medians_boot, sigmas_boot, boundaries_boot):

	# create new .csv file
	with open('results_'+image_name+'_n='+str(n)+'.csv', 'w') as csvfile:

			# create writer object
			writer = csv.writer(csvfile)

			# write measurement details
			writer.writerow(['Image:', image_name, image_name, image_name, image_name, image_name])
			writer.writerow(['Flavor:', flavor_name, flavor_name, flavor_name, flavor_name, flavor_name])
			writer.writerow(['n:', str(n),str(n), str(n), str(n), str(n)])
			writer.writerow(['Interval:', interval, interval, interval, interval, interval])
			writer.writerow(['t:', str(t),str(t), str(t), str(t), str(t)])
			writer.writerow(['type:', 'Func', 'Init', 'NC', 'NC With Date', 'Logfile'])

			# write medians
			writer.writerow(['Median [s]:', str(medians_init[0]), str(medians_init[1]), str(medians_boot[0]), str(medians_boot[1]), str(medians_boot[2])])

			# write interval
			writer.writerow(['CI [s]:', '+/- '+str(boundaries_init[0]), '+/- '+str(boundaries_init[1]), '+/- '+str(boundaries_boot[0]), '+/- '+str(boundaries_boot[1]), '+/- '+str(boundaries_boot[2])])

			# write sigmas
			writer.writerow(['SD [s]:', str(sigmas_init[0]), str(sigmas_init[1]), str(sigmas_boot[0]), str(sigmas_boot[1]), str(sigmas_boot[2])])

			# write first boot time
			writer.writerow(['Values [s]:', str(time_values_f[0]), str(time_values_i[0]), str(time_values_b[0][0]), str(time_values_b[1][0]), str(time_values_b[2][0])])

			# write the rest of the boot times
			i = 1
			while i <= (n-1):

				writer.writerow(['',str(time_values_f[i]), str(time_values_i[i]), str(time_values_b[0][i]), str(time_values_b[1][i]), str(time_values_b[2][i])])
				i = i+1


def calc_interval(sigma, n, t):
    
    boundary = (t * sigma) / (n**(0.5))
        
    return boundary