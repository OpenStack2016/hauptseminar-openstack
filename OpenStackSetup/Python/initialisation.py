import methods_general as mg

##################################################################
#########Directories have to be adjusted##########################
##################################################################

security_group_name = 'default'
key_name = 'test_key'
imagefile = r'/home/entmpr11/OpenStack/ubuntukodo.img'
imagename = "Ubuntu_Kodo"
keypath = r'/home/entmpr11/OpenStack/PythonM/keypair1.pub'
default_group = mg.find_securityGroup(nova,security_group_name)

##################################################################
##################################################################

nova, glance = mg.authenticate()

mg.create_securityGroupRule(nova, default_group.id,"icmp",-1,-1)
mg.create_securityGroupRule(nova, default_group.id,"tcp",22,22)

mg.import_keyPairs(nova,keypath,keyname)

mg.create_image(glance,imagefile,imagename)

mg.create_instances(nova,'instance',imagename,m1.small,key_name,1)

mg.create_until_i_fips(nova,3)